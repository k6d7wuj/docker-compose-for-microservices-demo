#!/bin/sh

echo "
# Настройки API
server {
    listen      80;
    server_name $USER_SERVICE_HOSTNAME;

    index index.php;
    root /var/www/html/public/;
    client_max_body_size 15m;

    access_log /var/log/nginx/api_access.log;
    error_log /var/log/nginx/api_error.log;

    location / {
        try_files \$uri /index.php\$is_args\$args;
    }

    location ~ \.php$ {
        try_files \$uri \$uri/ =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME \$fastcgi_script_name;
        fastcgi_index index.php;
        fastcgi_pass user:9000;
    }

}
" >> /etc/nginx/conf.d/nginx.default.conf

